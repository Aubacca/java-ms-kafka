package com.trivadis.customer.commands;

import com.trivadis.customer.services.CustomerEventsProducerService;
import com.trivadis.core.command.Command;
import com.trivadis.core.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RegisterCustomerCommand implements Command {

    private Customer customer;
    private CustomerEventsProducerService customerEventsService;

    @Autowired
    public RegisterCustomerCommand(final CustomerEventsProducerService customerEventsService) {
        log.debug("RegisterCustomerCommand.ctor>");
        this.customerEventsService = customerEventsService;
        log.debug("RegisterCustomerCommand.ctor<");
    }

    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    @Override
    public void execute() {
        log.debug("RegisterCustomerCommand.execute>");
        if (customer == null) {
            throw new IllegalArgumentException("Customer to be registered must be defined");
        }
        customerEventsService.registerCustomer(customer);
        log.debug("RegisterCustomerCommand.execute>");
    }
}
