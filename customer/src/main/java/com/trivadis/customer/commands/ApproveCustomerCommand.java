package com.trivadis.customer.commands;

import com.trivadis.customer.queries.CustomerByIdQuery;
import com.trivadis.customer.storage.CustomerStoreService;
import com.trivadis.core.command.Command;
import com.trivadis.core.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ApproveCustomerCommand implements Command {
    private final CustomerByIdQuery customerByIdQuery;
    private final CustomerStoreService customerStoreService;
    private Customer customer;

    @Autowired
    public ApproveCustomerCommand(CustomerByIdQuery customerByIdQuery, CustomerStoreService customerStoreService) {
        this.customerByIdQuery = customerByIdQuery;
        this.customerStoreService = customerStoreService;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void execute() {
        log.debug("ApproveCustomerCommand.execute>customer=" + customer);
        // Is a customer passed to be approved?
        if (customer == null) {
            log.error("A customer must be defined to be approved.");
            throw new IllegalArgumentException("A customer must be defined to be approved.");
        }
        //
        // Find customer to be approved in the list of registered customers.
        customerByIdQuery.setCustomerId(customer.getId());
        final Customer registeredCustomer = customerByIdQuery.query();
        log.debug("ApproveCustomerCommand.execute>registeredCustomer=" + registeredCustomer);
        if (registeredCustomer.getId() == null) {
            log.error("This customer is not registered in the system", customer);
            throw new IllegalArgumentException("This customer is not registered in the system: customer=" + customer);
        }
        //
        // Remove old customer data from store and add new customer data into storage.
        customerStoreService.getAllCustomers().remove(registeredCustomer);
        customerStoreService.getCustomerMap().put(customer.getId(), customer);
        log.debug("ApproveCustomerCommand.execute<");
    }
}
