package com.trivadis.customer.config;

import com.trivadis.customer.events.CustomerEvent;
import com.trivadis.customer.events.CustomerKafkaValueDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableKafka
@Slf4j
public class KafkaConsumerConfiguration {
    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Bean
    public ConsumerFactory<String, CustomerEvent> customerEventConsumerFactory() {
        log.debug("KafkaConsumerConfiguration.customerEventConsumerFactory>");
        Map<String, Object> kafkaConfig = new HashMap<>();
        log.debug("KafkaConsumerConfiguration.customerEventConsumerFactory>bootstrapAddress=" + bootstrapAddress);
        kafkaConfig.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        kafkaConfig.put(ConsumerConfig.GROUP_ID_CONFIG, "registration-customer-registered");
        kafkaConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        kafkaConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        kafkaConfig.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        kafkaConfig.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerKafkaValueDeserializer.class);
        log.debug("KafkaConsumerConfiguration.customerEventConsumerFactory>kafkaConfig=" + kafkaConfig);
        log.debug("KafkaConsumerConfiguration.customerEventConsumerFactory<");
        return new DefaultKafkaConsumerFactory<>(kafkaConfig, new StringDeserializer(), new CustomerKafkaValueDeserializer());
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> customerEventKafkaListenerContainerFactory() {
        log.debug("KafkaConsumerConfiguration.kafkaListenerCustomerContainerFactory>");
        ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(customerEventConsumerFactory());
        log.debug("KafkaConsumerConfiguration.kafkaListenerCustomerContainerFactory<");
        return factory;
    }

    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        log.debug("KafkaConsumerConfiguration.consumerFactory>");
        Map<String, Object> kafkaProps = new HashMap<>();
        kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-group-id");
        kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        log.debug("KafkaConsumerConfiguration.consumerFactory<");
        return new DefaultKafkaConsumerFactory<>(kafkaProps);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        log.debug("KafkaConsumerConfiguration.kafkaListenerContainerFactory>");
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        log.debug("KafkaConsumerConfiguration.kafkaListenerContainerFactory<");
        return factory;
    }
}
