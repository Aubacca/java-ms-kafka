package com.trivadis.customer.queries;

import com.trivadis.customer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import com.trivadis.core.query.QueryList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllCustomerQueryList implements QueryList<Customer> {
    private CustomerStoreService customerStoreService;

    @Autowired
    public AllCustomerQueryList(CustomerStoreService customerStoreService) {
        this.customerStoreService = customerStoreService;
    }

    @Override
    public List<Customer> queryList() {
        return new ArrayList<>(customerStoreService.getAllCustomers());
    }
}
