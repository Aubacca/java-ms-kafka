package com.trivadis.customer.queries;

import com.trivadis.customer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import com.trivadis.core.query.Query;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;

@Component
@Slf4j
public class CustomerByIdQuery implements Query<Customer> {
    private CustomerStoreService customerStoreService;
    private UUID customerId;

    @Autowired
    public CustomerByIdQuery(CustomerStoreService customerStoreService) {
        this.customerStoreService = customerStoreService;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    @Override
    public Customer query() {
        log.debug("CustomerByIdQuery.query>customerId=" + customerId);
        if (customerId == null) {
            throw new IllegalArgumentException("Customer ID must be define but is null");
        }
        final Collection<Customer> allCustomers = customerStoreService.getAllCustomers();
        log.debug("CustomerByIdQuery.query>allCustomers=" + allCustomers);
        final Customer customer = allCustomers.stream()
                .filter(c -> customerId.equals(c.getId()))
                .findFirst()
                .orElse(new Customer("", ""));
        log.debug("CustomerByIdQuery.query>customer=" + customer);
        log.debug("CustomerByIdQuery.query<");
        return customer;
    }
}

