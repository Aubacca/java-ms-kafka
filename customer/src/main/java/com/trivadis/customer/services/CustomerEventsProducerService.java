package com.trivadis.customer.services;

import com.trivadis.customer.events.CustomerEvent;
import com.trivadis.customer.events.CustomerEventProducer;
import com.trivadis.customer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerEventsProducerService {
    // Temporary data store for customers.
    private CustomerStoreService customerStoreService;
    // Kafka event producer.
    private CustomerEventProducer eventProducer;

    @Autowired
    public CustomerEventsProducerService(CustomerStoreService customerStoreService, CustomerEventProducer eventProducer) {
        this.customerStoreService = customerStoreService;
        this.eventProducer = eventProducer;
    }

    public void registerCustomer(Customer customer) {
        log.debug("CustomerEventsProducerService.registerCustomer>customer=" + customer.toString());
        customerStoreService.register(customer.getId(), customer);
        //
        // Publish new registered customer in kafka.
        eventProducer.publish(new CustomerEvent(customer));
        //
        // Show list of all customers.
        log.debug("CustomerEventsProducerService.registerCustomer>allCustomers=" + customerStoreService.getAllCustomers());
        log.debug("CustomerEventsProducerService.registerCustomer<");
    }
}
