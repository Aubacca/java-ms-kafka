package com.trivadis.customer.events;

import com.trivadis.core.model.Customer;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class CustomerEvent {
    private final Customer customer;
}
