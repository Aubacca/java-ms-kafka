package com.trivadis.customer.events;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

@Slf4j
public class CustomerKafkaValueDeserializer implements Deserializer<CustomerEvent> {

    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public CustomerEvent deserialize(String topic, byte[] bytes) {
        log.debug("CustomerKafkaValueDeserializer.deserialize>topic=" + topic);
        log.debug("CustomerKafkaValueDeserializer.deserialize>bytes=" + bytes);
        final CustomerEvent customerEvent = new Gson().fromJson(new String(bytes), CustomerEvent.class);
        log.debug("CustomerKafkaValueDeserializer.deserialize>customerEvent.getCustomer()=" + customerEvent.getCustomer());
        log.debug("CustomerKafkaValueDeserializer.deserialize<");
        return customerEvent;
    }

    @Override
    public void close() {

    }
}
