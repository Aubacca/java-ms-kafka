package com.trivadis.customer.events;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

@Slf4j
public class CustomerEventSerializer implements Serializer<CustomerEvent> {
    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public byte[] serialize(String topic, CustomerEvent customerEvent) {
        log.debug("CustomerEventSerializer.serialize>topic=" + topic);
        log.debug("CustomerEventSerializer.serialize>customerEvent=" + customerEvent);
        final String customerJson = new Gson().toJson(customerEvent);
        log.debug("CustomerEventSerializer.serialize>customerJson=" + customerJson);
        log.debug("CustomerEventSerializer.serialize>");
        return customerJson.getBytes();
//        return new byte[0];
    }

    @Override
    public void close() {

    }
}
