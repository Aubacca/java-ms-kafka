package com.trivadis.customer.events;

import com.trivadis.customer.commands.ApproveCustomerCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerEventConsumer {
    private final ApproveCustomerCommand approveCustomerCommand;
    @Value(value = "${spring.kafka.topic}")
    private String topic;

    @Autowired
    public CustomerEventConsumer(ApproveCustomerCommand approveCustomerCommand) {
        this.approveCustomerCommand = approveCustomerCommand;
    }

    @KafkaListener(
            topics = CustomerEventProducer.TOPIC,
            groupId = "registration-customer-registered",
            containerFactory = "customerEventKafkaListenerContainerFactory")
    public void onCustomerEvent(CustomerEvent customerEvent) {
        log.debug("CustomerEventConsumer.onCustomerEvent>customerEvent=" + customerEvent.getCustomer());
        switch (customerEvent.getCustomer().getStatus()) {
            case APPROVED:
                // Set customer as successfully approved to this system.
                approveCustomerCommand.setCustomer(customerEvent.getCustomer());
                approveCustomerCommand.execute();
                break;
            case REGISTERED:
                // Do nothing if customer status is REGISTERED!
                break;
            default:
                log.debug("CustomerEventConsumer.onCustomerEvent>Unhandled customer event: customer=" + customerEvent.getCustomer());
        }
    }
}
