package com.trivadis.customer.events;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.ProducerFencedException;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

@Component
@Slf4j
public class CustomerEventProducer {
    private KafkaProducer<String, CustomerEvent> kafkaProducer;
    public static final String TOPIC = "customer";

    public CustomerEventProducer() {
        initKafkaProducer();
    }

    private void initKafkaProducer() {
        log.debug("EventProducer.initKafkaProducer>");
        try {
            Properties kafkaProperties = new Properties();
            kafkaProperties.load(CustomerEventProducer.class.getResourceAsStream("/kafka.properties"));
            //
            kafkaProperties.put("transactional.id", UUID.randomUUID().toString());
//            kafkaProperties.put("auto.offset.reset", "earliest");
//            kafkaProperties.put("enable.auto.commit", false);
//            kafkaProperties.put("isolation.level", "read_committed");
            kafkaProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            kafkaProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomerEventSerializer.class);
            kafkaProducer = new KafkaProducer<>(kafkaProperties);
            kafkaProducer.initTransactions();
            log.debug("EventProducer.initKafkaProducer>kafkaProperties=" + kafkaProperties);
        } catch (IOException e) {
            log.error("EventProducer.initKafkaProducer>e=" + e);
            throw new IllegalStateException(e);
        }
        log.debug("EventProducer.initKafkaProducer<");
    }

    public void publish(CustomerEvent... events) {
        log.debug("EventProducer.publish>");
        try {
            kafkaProducer.beginTransaction();
            send(events);
            kafkaProducer.commitTransaction();
        } catch (ProducerFencedException e) {
            log.error("EventProducer.publish>ProducerFencedException: e=" + e);
            kafkaProducer.close();
        } catch (KafkaException e) {
            log.error("EventProducer.publish>KafkaException: e=" + e);
            kafkaProducer.abortTransaction();
        }
        log.debug("EventProducer.publish<");
    }

    private void send(CustomerEvent... events) {
        log.debug("EventProducer.send>");
        for (final CustomerEvent event : events) {
            final ProducerRecord<String, CustomerEvent> record = new ProducerRecord<>(TOPIC, "myKeyForProducerRecord", event);
            log.info("EventProducer.send>record=" + record);
            kafkaProducer.send(record);
        }
        log.debug("EventProducer.send<");
    }
}
