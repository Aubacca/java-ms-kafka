package com.trivadis.customer.api;

import com.trivadis.customer.commands.RegisterCustomerCommand;
import com.trivadis.customer.queries.AllCustomerQueryList;
import com.trivadis.core.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "api/v1/customers", consumes = "application/json", produces = "application/json")
@Slf4j
public class CustomerApi {
    private RegisterCustomerCommand registerCustomerCommand;
    private AllCustomerQueryList allCustomerQueryList;

    public CustomerApi(RegisterCustomerCommand registerCustomerCommand, AllCustomerQueryList allCustomerQueryList) {
        this.registerCustomerCommand = registerCustomerCommand;
        this.allCustomerQueryList = allCustomerQueryList;
    }

    @GetMapping("{payload}")
    public ResponseEntity<String> testEndpoint(@PathVariable String payload) {
        log.debug("CustomerApi.testEndpoint>payload=" + payload);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("CustomerApi.testApi>payload=" + payload);
    }

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers() {
        return ResponseEntity.status(HttpStatus.OK).body(allCustomerQueryList.queryList());
    }

    @PostMapping("register")
    public ResponseEntity<Customer> registerCustomer(@RequestBody Customer customer) {
        log.debug("CustomerApi.registerCustomer>customer=" + customer);
        //
        // Check for minimum customer data before process with customer registration.
        if (customer.getFirstName() == null || customer.getLastName() == null || customer.getEmail() == null) {
            log.error("Register Customer: Not all required data received to register a new customer: " + customer.toString());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(customer);
        }
        // Create new id for new customer
        customer.setId(UUID.randomUUID());
        //
        // Add customer to storage.
        registerCustomerCommand.setCustomer(customer);
        registerCustomerCommand.execute();
        //
        // Send acknowledge to requester.
        log.debug("CustomerApi.registerCustomer<");
        //
        // Acknowledge execution of this end point.
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(customer);
    }
}
