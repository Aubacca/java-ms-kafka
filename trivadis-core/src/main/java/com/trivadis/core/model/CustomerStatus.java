package com.trivadis.core.model;

public enum CustomerStatus {
    REGISTERED,
    APPROVED,
    DEACTIVATED,
    WAITING_FOR_REGISTRATION_CONFIRMATION;
}
