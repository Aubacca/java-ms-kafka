package com.trivadis.core.model;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
//@NoArgsConstructor
@RequiredArgsConstructor
//@AllArgsConstructor(staticName = "of")
//@Builder
@ToString
public class Customer {

    private UUID id;
    private final String firstName;
    private final String lastName;
//    private String firstName;
//    private String lastName;
    private String email;
    private CustomerStatus status = CustomerStatus.REGISTERED;
}

