package com.trivadis.core.query;

/**
 * CQRS - query for a single result.
 */
public interface Query<R> {
    R query();
}
