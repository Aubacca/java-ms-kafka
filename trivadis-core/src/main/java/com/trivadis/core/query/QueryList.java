package com.trivadis.core.query;

import java.util.List;

/**
 * CQRS - query for a list of results.
 */
public interface QueryList<R> {
    List<R> queryList();
}
