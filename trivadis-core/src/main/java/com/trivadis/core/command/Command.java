package com.trivadis.core.command;

/**
 * CQRS - command.
 */
public interface Command {
    void execute();
}
