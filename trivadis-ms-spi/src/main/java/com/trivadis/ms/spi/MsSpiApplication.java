package com.trivadis.ms.spi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsSpiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsSpiApplication.class, args);
	}

}
