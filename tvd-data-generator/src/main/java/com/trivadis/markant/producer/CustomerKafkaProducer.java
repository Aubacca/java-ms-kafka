package com.trivadis.markant.producer;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.trivadis.core.model.Customer;
import com.trivadis.markant.utils.JsonNodeSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.List;
import java.util.Properties;

import static org.apache.kafka.clients.producer.ProducerConfig.*;

public class CustomerKafkaProducer {
    private final String topic;
    private final List<Customer> customerList;
    private static Gson gson = new Gson();

    public CustomerKafkaProducer(String topic, List<Customer> customerList) {
        this.topic = topic;
        this.customerList = customerList;
    }

    private Producer<String, JsonObject> getProducer() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ACKS_CONFIG, "all");
        props.put(RETRIES_CONFIG, 0);
        props.put(BATCH_SIZE_CONFIG, 16000);    // Size in bytes.
        props.put(LINGER_MS_CONFIG, 100);   // Waiting time in ms.
//        props.put(BUFFER_MEMORY_CONFIG, 33554432);
        props.put(BUFFER_MEMORY_CONFIG, 30000000);
        props.put(KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, JsonNodeSerializer.class);
//        props.put(VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //
        return new KafkaProducer<>(props);
    }

    private JsonObject createWrapper(Customer customer) {
        JsonObject userJson = new JsonObject();
        userJson.addProperty("action", "customer.create");
        userJson.add("object", gson.toJsonTree(customer));
        //
        return userJson;
    }

    public void sendData() {
        // Measure the starting time.
        long t1 = System.currentTimeMillis();
        //
        Producer<String, JsonObject> producer = getProducer();
//        Producer<String, String> producer = getProducer();
        // Create a list of customers.
        List<Customer> customers = this.customerList;
        //
        for (Customer customer : customers) {
             producer.send(new ProducerRecord<>(this.topic, customer.getId().toString(), createWrapper(customer)));
        }
        //
        producer.close();
        //
        // Show total records sent to producer and time to execute them with Kafka.
        System.out.format("CustomerKafkaProducer.sendData>Finished after %.3f s for %d records", ((System.currentTimeMillis() - t1) / 1000.0), customers.size());
    }
}
