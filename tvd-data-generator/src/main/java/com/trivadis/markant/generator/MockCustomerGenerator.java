package com.trivadis.markant.generator;

import com.trivadis.core.model.Customer;

import java.util.*;

public class MockCustomerGenerator {

    private final int numberOfRecords;

    private Random rand = new Random();
    private final static List<String> firstNameList = Arrays.asList("Adam", "Thomas", "Simone", "Monika", "Claudia", "Heiko", "Frank", "Sara", "Daniela");
    private final static List<String> lastNameList = Arrays.asList("Meier", "Freiberg", "Graf", "Ritter", "Wenger", "Engelberg", "Muster", "Reiner");
    private final static List<String> mailProviderList = Arrays.asList("gmail.com", "microsoft.org", "telecom.de", "yahoo.com", "freenet.de");

    public MockCustomerGenerator(int numberOfRecords) {
        this.numberOfRecords = numberOfRecords;
    }

    public List<Customer> getList() {
        final List<Customer> customerList = new ArrayList<>();
        for (int i = 0; i < this.numberOfRecords; i++) {
            int randomIndex = rand.nextInt(firstNameList.size());
            String firstName = firstNameList.get(randomIndex);
            randomIndex = rand.nextInt(lastNameList.size());
            String lastName = lastNameList.get(randomIndex);
            randomIndex = rand.nextInt(mailProviderList.size());
            String providerName = mailProviderList.get(randomIndex);
            final Customer customer = new Customer(firstName, lastName);
            customer.setId(UUID.randomUUID());
            customer.setEmail((firstName + "." + lastName + "@" + providerName).toLowerCase());
            customerList.add(customer);
//
//            Customer.of(UUID.randomUUID(), firstName, lastName, (firstName + "." + lastName + "@" + providerName).toLowerCase());
//            Customer.builder().build();
        }
//        for (Customer customer : customerList) {
//            System.out.println("CustomerKafkaProducer>customer: " + customer.toString());
//        }
        //
        return customerList;
    }
}
