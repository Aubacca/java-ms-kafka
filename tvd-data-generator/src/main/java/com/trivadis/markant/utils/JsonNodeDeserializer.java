package com.trivadis.markant.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class JsonNodeDeserializer implements Deserializer {
    private final JsonParser jsonParser = new JsonParser();
    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public Object deserialize(String topic, byte[] bytes) {
        String json = new String(bytes);
        return (JsonObject)jsonParser.parse(json);
    }

    @Override
    public void close() {

    }
}
