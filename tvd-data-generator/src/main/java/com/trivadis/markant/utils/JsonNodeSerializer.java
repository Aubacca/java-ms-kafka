package com.trivadis.markant.utils;

import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JsonNodeSerializer implements Serializer<JsonObject> {

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String topic, JsonObject jsonObject) {
        return jsonObject.toString().getBytes();
    }

    @Override
    public void close() {

    }
}
