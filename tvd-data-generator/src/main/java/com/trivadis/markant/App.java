package com.trivadis.markant;

import com.trivadis.core.model.Customer;
import com.trivadis.markant.generator.MockCustomerGenerator;
import com.trivadis.markant.producer.CustomerKafkaProducer;

import java.util.List;

/**
 * Application to generate mocked customer data for Apache Kafka.
 */
public class App 
{

    public static final String TOPIC_NAME = "tvd-demo-cleanup";

    public static void main(String[] args )
    {
        // Generate customer data which will be sent to Kafka.
        final MockCustomerGenerator mockCustomerGenerator = new MockCustomerGenerator(10);
        final List<Customer> customerList = mockCustomerGenerator.getList();

        final CustomerKafkaProducer customerProducer = new CustomerKafkaProducer(TOPIC_NAME, customerList);
        customerProducer.sendData();
    }
}
