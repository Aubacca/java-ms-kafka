package com.trivadis.mailer.queries;

import com.trivadis.mailer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import com.trivadis.core.model.CustomerStatus;
import com.trivadis.core.query.QueryList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AllUnregisteredCustomerQueryList implements QueryList<Customer> {
    private CustomerStoreService customerStoreService;

    @Autowired
    public AllUnregisteredCustomerQueryList(CustomerStoreService customerStoreService) {
        this.customerStoreService = customerStoreService;
    }

    @Override
    public List<Customer> queryList() {
        final List<Customer> unregisteredCustomers = customerStoreService.getAllCustomers().stream()
                .filter(c -> c.getStatus() == CustomerStatus.WAITING_FOR_REGISTRATION_CONFIRMATION)
                .collect(Collectors.toList());
        return unregisteredCustomers;
    }
}
