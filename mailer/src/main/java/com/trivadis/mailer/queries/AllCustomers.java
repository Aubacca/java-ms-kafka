package com.trivadis.mailer.queries;

import com.trivadis.mailer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import com.trivadis.core.query.QueryList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllCustomers implements QueryList<Customer> {
    private CustomerStoreService customerStoreService;

    @Autowired
    public AllCustomers(CustomerStoreService customerStoreService) {
        this.customerStoreService = customerStoreService;
    }
    @Override
    public List<Customer> queryList() {
        return new ArrayList<>(customerStoreService.getAllCustomers());
    }
}
