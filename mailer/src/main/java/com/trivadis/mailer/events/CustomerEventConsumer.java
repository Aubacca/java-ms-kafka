package com.trivadis.mailer.events;

import com.trivadis.mailer.services.CustomerService;
import com.trivadis.core.model.CustomerStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerEventConsumer {
    private CustomerService customerService;

    @Autowired
    public CustomerEventConsumer(CustomerService customerService) {
        this.customerService = customerService;
    }

    @KafkaListener(topics = "customer", groupId = "customer-group-id", containerFactory = "allKafkaListenerContainerFactory")
    public void consumeCustomerEvent(CustomerEvent customerEvent) {
        log.debug("CustomerEventConsumer.consumeCustomerEvent>Customer mailer application!");
        log.debug("CustomerEventConsumer.consumeCustomerEvent>customerEvent=" + customerEvent);
        customerService.addUnregisteredCustomer(customerEvent);
    }

/*
    @KafkaListener(topics = "customer", groupId = "customer-registered-group", containerFactory = "filterKafkaListenerContainerFactory")
    public void onCustomerRegistered(CustomerEvent customerEvent) {
        log.debug("CustomerEventConsumer.onCustomerRegistered>Customer mailer application!");
        log.debug("CustomerEventConsumer.onCustomerRegistered>customerEvent=" + customerEvent.getCustomer());
        customerService.addUnregisteredCustomer(customerEvent);
    }*/
}
