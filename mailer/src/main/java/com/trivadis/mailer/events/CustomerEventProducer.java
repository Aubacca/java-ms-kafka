package com.trivadis.mailer.events;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerEventProducer {

    private final KafkaTemplate<String, CustomerEvent> kafkaTemplate;

    @Autowired
    public CustomerEventProducer(KafkaTemplate<String, CustomerEvent> kafkaTemplate) {
        log.debug("CustomerEventProducer.ctor>");
        this.kafkaTemplate = kafkaTemplate;
        log.debug("CustomerEventProducer.ctor<");
    }

    public void sendMessage(final CustomerEvent customerEvent) {
        log.debug("CustomerEventProducer.sendMessage>customerEvent=" + customerEvent.getCustomer());
        kafkaTemplate.send("customer", customerEvent);
        log.debug("CustomerEventProducer.sendMessage<" );
    }
}
