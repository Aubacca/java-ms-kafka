package com.trivadis.mailer.api;

import com.trivadis.mailer.commands.ConfirmCustomerRegistrationCommand;
import com.trivadis.mailer.events.CustomerEvent;
import com.trivadis.mailer.events.CustomerEventProducer;
import com.trivadis.mailer.queries.AllCustomers;
import com.trivadis.mailer.queries.AllUnregisteredCustomerQueryList;
import com.trivadis.mailer.queries.CustomerByIdQuery;
import com.trivadis.core.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "api/v1/mailer", produces = "application/json", consumes = "application/json")
public class MailerApi {
    private AllUnregisteredCustomerQueryList allUnregisteredCustomerQueryList;
    private CustomerByIdQuery customerByIdQuery;
    private ConfirmCustomerRegistrationCommand confirmCustomerRegistrationCommand;
    private AllCustomers allCustomers;
    private final CustomerEventProducer customerEventProducer;

    @Autowired
    public MailerApi(AllUnregisteredCustomerQueryList allUnregisteredCustomerQueryList,
                     CustomerByIdQuery customerByIdQuery,
                     ConfirmCustomerRegistrationCommand confirmCustomerRegistrationCommand,
                     AllCustomers allCustomers,
                     CustomerEventProducer customerEventProducer
    ) {
        this.allUnregisteredCustomerQueryList = allUnregisteredCustomerQueryList;
        this.customerByIdQuery = customerByIdQuery;
        this.confirmCustomerRegistrationCommand = confirmCustomerRegistrationCommand;
        this.allCustomers = allCustomers;
        this.customerEventProducer = customerEventProducer;
    }

    @GetMapping("unregistered-customers")
    public ResponseEntity<List<Customer>> getAllUnregisteredCustomers() {
        log.debug("MailerApi.getAllUnregisteredCustomers>");
        final List<Customer> unregisteredCustomers = allUnregisteredCustomerQueryList.queryList();
        log.debug("MailerApi.getAllUnregisteredCustomers>unregisteredCustomers=" + unregisteredCustomers);
        log.debug("MailerApi.getAllUnregisteredCustomers<");
        return ResponseEntity.status(HttpStatus.OK).body(unregisteredCustomers);
    }

    @GetMapping("all-customers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        log.debug("MailerApi.getAllCustomers>");
        final List<Customer> allCustomers = this.allCustomers.queryList();
        log.debug("MailerApi.getAllUnregisteredCustomers>allCustomers=" + allCustomers);
        log.debug("MailerApi.getAllCustomers<");
        return ResponseEntity.status(HttpStatus.OK).body(allCustomers);
    }

    @GetMapping("{customerId}/confirm-registration")
    public ResponseEntity<Customer> customerConfirmRegistration(@PathVariable UUID customerId) {
        log.debug("MailerApi.customerConfirmRegistration>customerId=" + customerId);
        confirmCustomerRegistrationCommand.setCustomerId(customerId);
        confirmCustomerRegistrationCommand.execute();
        // Get customer by id.
        customerByIdQuery.setCustomerId(customerId);
        final Customer customer = customerByIdQuery.query();
        //
        // Send customer was successfully registered.
        customerEventProducer.sendMessage(new CustomerEvent(customer));
        //
        // Acknowledge that this end point execution.
        log.debug("MailerApi.customerConfirmRegistration<");
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(customer);
    }
}
