package com.trivadis.mailer.services;

import com.trivadis.mailer.events.CustomerEvent;
import com.trivadis.mailer.queries.CustomerByIdQuery;
import com.trivadis.mailer.storage.CustomerStoreService;
import com.trivadis.core.model.Customer;
import com.trivadis.core.model.CustomerStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CustomerService {
    // Temporary data store for customers.
    private CustomerStoreService customerStoreService;
    private CustomerByIdQuery customerByIdQuery;

    @Autowired
    public CustomerService(CustomerStoreService customerStoreService, CustomerByIdQuery customerByIdQuery) {
        this.customerStoreService = customerStoreService;
        this.customerByIdQuery = customerByIdQuery;
    }

    public void addUnregisteredCustomer(CustomerEvent customerEvent) {
        log.debug("CustomerService.addUnregisteredCustomer>customerEvent=" + customerEvent);
        if (customerEvent == null || customerEvent.getCustomer() == null) {
            throw new IllegalArgumentException("CustomerService.addUnregisteredCustomer>Mailer - missing customer information: " + customerEvent);
        }
        Customer customer = customerEvent.getCustomer();
        log.debug("CustomerService.addUnregisteredCustomer>customer=" + customer);
        //
        // Must this customer confirm his registration?
        if (customer.getStatus() == CustomerStatus.REGISTERED) {
            // Are we already waiting for this customer to confirm his registration?
            customerByIdQuery.setCustomerId(customer.getId());
            final Customer storedCustomer = customerByIdQuery.query();
            log.debug("CustomerService.addUnregisteredCustomer>storedCustomer=" + storedCustomer);
            if (storedCustomer.getId() != null) {
                throw new IllegalArgumentException("Unknown customer or customer already stored to call to confirm registration: " + customer);
            }
            customer.setStatus(CustomerStatus.WAITING_FOR_REGISTRATION_CONFIRMATION);
            customerStoreService.addUnregistered(customer.getId(), customer);
        }
        log.debug("CustomerService.addUnregisteredCustomer>customer=" + customer);
        log.debug("CustomerService.addUnregisteredCustomer<");
    }

    public void doConfirmRegistration(Customer customer) {

    }

}
