package com.trivadis.mailer.storage;

import com.trivadis.core.model.Customer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * A temporary local store for customers of this application.
 */
@Service
@Slf4j
@Data
public class CustomerStoreService {
    // Temporary data store for customers.
    private Map<UUID, Customer> customerMap = new HashMap<>();


    public Customer addUnregistered(UUID id, Customer customer) {
       return customerMap.put(id, customer);
    }

    public Collection<Customer> getAllCustomers() {
        return customerMap.values();
    }
}
