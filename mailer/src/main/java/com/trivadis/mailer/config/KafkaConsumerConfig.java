package com.trivadis.mailer.config;

import com.trivadis.mailer.events.CustomerEvent;
import com.trivadis.mailer.events.CustomerKafkaValueDeserializer;
import com.trivadis.core.model.CustomerStatus;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
@Slf4j
public class KafkaConsumerConfig {
    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    private ConsumerFactory<String, CustomerEvent> consumerFactory() {
        Map<String, Object> kafkaProps = new HashMap<>();
        log.debug("KafkaConsumerConfig.consumerFactory>bootstrapAddress=" + bootstrapAddress);
        kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerKafkaValueDeserializer.class);
        //
        kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-group-id");
        kafkaProps.put("enable.auto.commit", "false");
        kafkaProps.put("auto.offset.reset", "earliest");
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        return new DefaultKafkaConsumerFactory<>(kafkaProps);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> allKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

 /*   @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }*/

    @Bean
    public KafkaTemplate<String, CustomerEvent> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    //    @Bean
    private ProducerFactory<String, CustomerEvent> producerFactory() {
        Map<String, Object> kafkaProps = new HashMap<>();
        kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(kafkaProps);
    }

/*
    private ConsumerFactory<String, CustomerEvent> consumerFilteredFactory() {
        Map<String, Object> kafkaProps = new HashMap<>();
        log.debug("KafkaConsumerConfig.consumerFactory>consumerFilteredFactory=" + bootstrapAddress);
        kafkaProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        kafkaProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        kafkaProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerKafkaValueDeserializer.class);
        //
        kafkaProps.put(ConsumerConfig.GROUP_ID_CONFIG, "customer-registered-group");
        kafkaProps.put("enable.auto.commit", "false");
        kafkaProps.put("auto.offset.reset", "earliest");
        return new DefaultKafkaConsumerFactory<>(kafkaProps);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> filterKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, CustomerEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFilteredFactory());
        factory.setRecordFilterStrategy(record -> {
                    log.debug("KafkaConsumerConfig.filterKafkaListenerContainerFactory>record=" + record.value().getCustomer());
                    return record.value().getCustomer().getStatus().equals(CustomerStatus.REGISTERED);
                }
        );
        return factory;
    }
*/
}
