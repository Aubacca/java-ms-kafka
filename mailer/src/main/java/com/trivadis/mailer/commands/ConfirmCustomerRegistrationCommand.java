package com.trivadis.mailer.commands;

import com.trivadis.mailer.queries.CustomerByIdQuery;
import com.trivadis.mailer.services.CustomerService;
import com.trivadis.mailer.storage.CustomerStoreService;
import com.trivadis.core.command.Command;
import com.trivadis.core.model.Customer;
import com.trivadis.core.model.CustomerStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Slf4j
public class ConfirmCustomerRegistrationCommand implements Command {
    private CustomerService customerService;
    private CustomerByIdQuery customerByIdQuery;
    private CustomerStoreService customerStoreService;
    private UUID customerId;

    @Autowired
    public ConfirmCustomerRegistrationCommand(CustomerService customerService, CustomerByIdQuery customerByIdQuery, CustomerStoreService customerStoreService) {
        this.customerService = customerService;
        this.customerByIdQuery = customerByIdQuery;
        this.customerStoreService = customerStoreService;
    }

    public void setCustomerId(UUID customerId) {
        log.debug("ConfirmCustomerRegistrationCommand.setCustomerId>customerId=" + customerId);
        this.customerId = customerId;
        customerByIdQuery.setCustomerId(customerId);
        log.debug("ConfirmCustomerRegistrationCommand.setCustomerId<");
    }

    @Override
    public void execute() {
        // Find customer by Id.
        final Customer foundCustomer = customerByIdQuery.query();
        log.debug("ConfirmCustomerRegistrationCommand.execute>foundCustomer=" + foundCustomer);
        //
        // Error handling.
        if (foundCustomer.getId() == null) {
            throw new IllegalArgumentException("Unknown customer or customer does not need to be confirmed.");
        }
        //
        // Change customer status as successfully approved.
        foundCustomer.setStatus(CustomerStatus.APPROVED);
        //
        // Create new Kafka event that this customer did successfully confirm his registration.
        //
        // Remove this customer from local storage as this customer was successfully confirmed.
//        customerStoreService.getAllCustomers().remove(foundCustomer);
        log.debug("ConfirmCustomerRegistrationCommand.execute>foundCustomer=" + foundCustomer);
    }
}
