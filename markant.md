# Markant - Blueprint Sample Code

## Szenario

In diesem Code Beispiel wird davon ausgegangen, dass sich ein Kunde via Internet selber registrieren kann. Nach seiner online Registrierung erhält er automatisch eine Mail in welcher sich ein Link befindet, welcher der Kunde anklicken muss, um seine Registrierung zu bestätigen und abzuschliessen.

## Code Beispiel

In diesem Code-Beispiel gibt es folgende Komponenten:
- ein Spring Boot Micro-Service __Customer__: hier würden sich neue Kunden zum ersten mal beim System anmelden. Auch hat man hier die Möglichkeit eine Liste alle registrierten oder bestätigten Kunden zu holen.
- ein Spring Boot Micro-Service __Mailer__: von hier würden neu registrierte Kunden eine Mail bekommen, mit welcher sie ihre Anmeldung nochmals bestätigen müssten. Ebenfalls lässt sich hier eine Liste aller Kunden die sich noch bestätigen müssen zu laden. Des Weitere bekommt man von hier eine Liste mit allen Kunden die ihre Registrierung noch bestätigen müssen oder dies erfolgreich getan haben.
- Kafka wird hier als Event Source verwendet via welchem die Micro-Services untereinander kommunizieren können.
- Beide Micro-Services besitzen einen Kafka Producer um neue Events zu erstellen und einen Kafka Consumer um auf neue Events zu reagieren.

## Zookeeper

Ein Zookeeper muss gestartet sein.

## Kafka Broker

Ein Kafka Broker muss gestartet sein und via localhost:9092 verfügbar sein.

## Customer [localhost:8010]

Hiermit würde sich eine neuer Kunde beim System zum ersten mal registrieren. Ebenfalls findet man hier heraus, ob man seine Registrierung erfolgreich bestätigt hat. Nur ein bestätigter Kunde (Status APPROVED) sollte im System weiter verwendet werden.

Bei diesem Micro-Service stehen folgende zwei Endpunkte zum Aufruf zur Verfügung.

### GET - api/v1/customers

Gibt eine JSON Liste mit allen Kunden zurück die registriert (REGISTERED) oder bestätigt (APPROVED) sind.

Request Beispiel:

````bash
curl -v http://localhost:8010/api/v1/customers -H "Content-Type: application/json"
````

Response Beispiel:

````json
[
  {
    "id": "6b460267-b7c8-4691-a127-fc299b208025",
    "firstName": "Simona",
    "lastName": "Byte",
    "email": "simona.byte@telecom.de",
    "status": "APPROVED"
  },
  {
    "id": "a9c7b250-ba19-4e59-bbdd-076cb0888d94",
    "firstName": "Max",
    "lastName": "Muster",
    "email": "m.muster@gmail.com",
    "status": "REGISTERED"
  }
]
````

### POST - api/v1/customers/register

Durch die Angabe seiner Informationen kann sich ein Kunde bei System registrieren.

Request Beispiel:

````bash
curl -v http://localhost:8010/api/v1/customers/register -d '{"firstName": "Max", "lastName": "Muster", "email": "m.muster@gmail.com"}' -H "Content-Type: application/json"
````

Als Response erhält man das JSON Objekt mit einer ID und dem Status des Kunden zurück.

Response Beispiel:

````json
{
"id":"a9c7b250-ba19-4e59-bbdd-076cb0888d94",
"firstName":"Max",
"lastName":"Muster",
"email":"m.muster@gmail.com",
"status":"REGISTERED"
}
````

## Mailer [localhost:8020]

Hier würde dem Kunde nach seiner Registierung eine Mail zugeschickt mit einem Hyperlink darin. Den Hyperlink müsste der Kunde ausführen umd seine Registrierung beim System zu bestätigen.

Bei diesem Micro-Service stehen folgende Endpunkte zum Aufruf zur Verfügung.

### GET - api/v1/mailer/unregistered-customers

Gibt eine JSON Liste aller Kunden zurück, welche ihre Registrierung im System noch nicht bestätigt haben.

Request Beispiel:

````bash
curl -v http://localhost:8020/api/v1/mailer/unregistered-customers -H "Content-Type: application/json"
````

Response Beispiel:

````json
[
  {
    "id": "a9c7b250-ba19-4e59-bbdd-076cb0888d94",
    "firstName": "Max",
    "lastName": "Muster",
    "email": "m.muster@gmail.com",
    "status": "WAITING_FOR_REGISTRATION_CONFIRMATION"
  },
  {
    "id": "ad231127-aa76-4925-8116-c53b1de65040",
    "firstName": "Janik",
    "lastName": "Dreier",
    "email": "dreierj@trivadis.com",
    "status": "WAITING_FOR_REGISTRATION_CONFIRMATION"
  }
]
````

### GET - api/v1/mailer/all-customers

Gibt eine JSON Liste mit allen Kunden zurück die noch bestätigt werden müssen oder erfolgreich bestätigt worden sind.

Request Beispiel:

````bash
curl -v http://localhost:8020/api/v1/mailer/all-customers -H "Content-Type: application/json"
````

Response Beispiel:

````json
[
  {
    "id": "6b460297-b7c8-4691-a127-fc299b208025",
    "firstName": "Frank",
    "lastName": "Spielmann",
    "email": "frank-spielmann@yahoo.com",
    "status": "APPROVED"
  },
  {
    "id": "a9c7b250-ba19-4e59-bbdd-076cb0888d94",
    "firstName": "Max",
    "lastName": "Muster",
    "email": "m.muster@gmail.com",
    "status": "WAITING_FOR_REGISTRATION_CONFIRMATION"
  },
  {
    "id": "ad231127-aa76-4925-8116-c53b1de65040",
    "firstName": "Janik",
    "lastName": "Dreier",
    "email": "dreierj@trivadis.com",
    "status": "WAITING_FOR_REGISTRATION_CONFIRMATION"
  }
]
````

### POST - http://localhost:8020/api/v1/mailer/{customer-id}/confirm-registration

Mit diesem Endpunkt kann der Kunde seine Registrierung bestätigen und abschliessen.
Als Antwort auf diesen REST Endpunkt erhält man das bearbeitete JSON Objekt zurück.

Request Beispiel:

````bash
curl -v http://localhost:8020/api/v1/mailer/a9c7b250-ba19-4e59-bbdd-076cb0888d94/confirm-registration -H "Content-Type: application/json"
````

Response Beispiel:

````json
{
  "id": "a9c7b250-ba19-4e59-bbdd-076cb0888d94",
  "firstName": "Max",
  "lastName": "Muster",
  "email": "m.muster@gmail.com",
  "status": "APPROVED",
}

````